#!/usr/bin/env bash
BASEDIR=$(dirname "$0");
sudo docker build --no-cache -t click_ranker:latest $BASEDIR;
docker run --rm -d -p 5000:5000 --name click_ranker click_ranker:latest;