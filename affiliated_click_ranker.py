import os
import numpy as np
import pickle
import spacy

class AffiliatedClickRanker:
    def __init__(self, model_path):
        self.nlp = spacy.load("en_core_web_sm")
        self.vspacy_ndim = len(self.nlp("random text").vector)
        self.model = pickle.load(open(model_path, "rb"))

    def string_to_vec(self, string):
        return self.nlp(string).vector

    def predict(self, name_arr):
        x = np.asarray([self.string_to_vec(name) for name in name_arr])
        y_prob = self.model.predict_proba(x)
        y_prob_true = y_prob[:,1]
        return y_prob_true

def main():
    test_arr = [
        "Biomed Ltd Escapes",
        "The Biomed Ltd",
        "Biomed Ltdly",
        "Lifelong Biomed Ltd",
        "Co Biomed Ltd",
        "Biomed Ltdism",
        "Biomed Ltd Garden",
        "Re Biomed Ltd",
        "Biomed Ltdfy",
        "Revitalizing Biomed Ltd",
        "My Biomed Ltd",
        "Biomed Ltdous",
        "Joyful Biomed Ltd",
        "We Biomed Ltd",
        "In Biomed Ltd",
        "Choice Biomed Ltd",
        "Biomed Ltd Up",
        "Biomed Ltdcy",
        "Balanced Biomed Ltd",
        "Go Biomed Ltd",
        "Biomed Escapes",
        "Lifelong Biomed",
        "Revitalizing Ltd",
        "Co Biomed",
        "Ltd Hop",
        "Ltd Age",
        "Biomedism",
        "Revitalizing Biomed",
        "Joyful Biomed",
        "Inclusive Ltd",
        "Biomed Age",
        "Ltd Ace",
        "Ltd Air",
        "Biomedfy",
        "Five Star Biomed",
        "Biomed Happiness",
        "Heavenly Ltd",
        "Biomed Sign",
        "Ltd Ster",
        "Ltd State",
        "The Divine Goddess Llc"
    ]

    ranker = AffiliatedClickRanker(os.path.join("models", "model.pkl"))
    affiliated_click_ranking = ranker.predict(test_arr)

    sorted_indices = np.argsort(affiliated_click_ranking)[::-1]

    for i in range(len(affiliated_click_ranking)):
        print(f"'{test_arr[i]}' score: {affiliated_click_ranking[i]}")
    # for i in sorted_indices:
    #     print(f"'{test_arr[i]}' score: {affiliated_click_ranking[i]}")

if __name__ == "__main__":
    main()