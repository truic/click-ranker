FROM python:3.8-slim

RUN mkdir src
WORKDIR src/
COPY . .

RUN pip install --upgrade pip
RUN pip install -r requirements.txt 
RUN python -m spacy download en_core_web_sm

CMD [ "python", "-u", "app.py"]