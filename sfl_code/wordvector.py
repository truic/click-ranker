import logging
import pandas as pd
import spacy
import numpy as np

logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s",
                    level=logging.INFO)
logger = logging.getLogger(__name__)


class WordVector:
    """
    Class to convert words to vectors.
    """

    def __init__(self, model="en_core_web_sm"):
        """
        Initialize the WordVector class
        Parameters:
            model: (str, optional, default "en_core_web_sm") the name of the
            model file to be loaded.
        """
        self.nlp = spacy.load(model)

    def convert_by_file(self, filename, column, out_name=None):
        """
        Given input file name and column name, convert text in that column to
        vectors of floats.

        Parameters:
            filename: (str) input file name
            column: (str) input column name
            out_name: (str, optional, default None) if it is not None, then the
            resulting file with the vectors are saved in this file name.

        Returns:
            dataframe with converted vector of floats
        """
        df = pd.read_csv(filename)
        logger.info("read file to dataframe: with column: %s" % column)
        return self.convert(df, column, out_name=out_name)

    def _str_to_vector(self, x, length):
        """
        Convert text to vector of floats.
        Parameters:
            x: (str) input text
            length: (int) length of the output vector, if input text is Nan,
            then return a vector of zeros with given length.
        """
        if isinstance(x, str):
            return self.nlp(x).vector
        elif np.isnan(x):
            logger.warning("Input: is Nan!")
            return np.zeros(length)
        else:
            logger.warning("Input:", x, "is not string!")
            try:
                return self.nlp(str(x)).vector
            except Exception:
                return np.zeros(length)

    def convert(self, df, column, out_column_name="company", out_name=None):
        """
        Given input dataframe and column name, convert text in that column to
        vectors of floats.

        Parameters:
            df: (pd.DataFrame) input dataframe
            column: (str) input column name
            out_column_name: (str, optional, default "company") if it is not
            None, then the output dataframe will use this column name for
            company names.
            out_name: (str, optional, default None) if it is not None, then the
            resulting file with the vectors are saved in this file name.

        Returns:
            dataframe with converted vector of floats
        """
        logger.info("start to convert text to vector with spacy.")
        vspacy_ndim = len(self.nlp("random text").vector)
        logger.info("Separate nlp vector into %d columns" % vspacy_ndim)
        assert column in df.columns, "%s not found in input!" % column
        df = df[[column]].copy()
        df = df.dropna()
        logger.info("Input size: %d" % len(df))
        df["X"] = df[column].apply(
            lambda x: self._str_to_vector(x, vspacy_ndim)
        )
        logger.info("Input texts are converted to vectors.")
        for i in range(vspacy_ndim):
            df["X_%d" % i] = df["X"].apply(lambda x: x[i])
        df.drop(columns=["X"], inplace=True)
        if out_column_name is not None:
            df.rename(columns={column: out_column_name}, inplace=True)

        if out_name is not None:
            logger.info("data name vector saved in: %s" % out_name)
            df.to_csv(out_name, index=False)

        return df
