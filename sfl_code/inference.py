import os
import pickle
import argparse
import toml
from time import time
from datetime import timedelta
import pandas as pd
import logging
from wordvector import WordVector

logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s",
                    level=logging.INFO)
logger = logging.getLogger(__name__)


class Inference:
    """
    Class to make XGBoost model inferences.
    """

    def __init__(self, config):
        """
        Initialization of the class Inference. The list of parameters in the
        configuration is listed:
            - model_file: (str) model file name
            - input_name: (str) input file name
            - column_name: (str) column name to be read from the input file
        """
        model_file = config.get("model_file", None)
        assert model_file is not None, "model file needed."
        self.model = pickle.load(open(model_file, "rb"))

        self.input_name = config.get("input_name", None)
        self.column_name = config.get("column_name", None)
        wv = WordVector()
        self.df = wv.convert_by_file(self.input_name, self.column_name)
        self.X = self.df[[c for c in self.df.columns if "X_" in c]]
        self.out_name = config.get("out_name", "output_inference.csv")
        # check if the output folder exists, if not, create one!
        outfold = "/".join(self.out_name.split("/")[0:-1])
        if outfold and (not os.path.exists(outfold)):
            os.makedirs(outfold)
            logger.info("Making dir: %s for output!" % outfold)

    def predict(self):
        """
        Test the XGBoost model and calculate the relevant performance metrics.
        """
        if self.model is None:
            logger.error("Model not found!")
            return None

        if self.X is None:
            logger.error("Test data is not found!")
            return None

        y_prob = self.model.predict_proba(self.X)
        y_prob_true = [v[1] for v in y_prob]
        y_pred = self.model.predict(self.X)
        predictions = [value for value in y_pred]
        df_out = pd.DataFrame({
            "company": self.df["company"].values,
            "confidence": y_prob_true,
            "prediction": predictions
        })
        df_out.to_csv(self.out_name, index=False)
        print(df_out)
        logger.info("Inference output saved to: %s" % self.out_name)


def main():
    t_start = time()
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--config", "-c", default="config.toml", type=str, required=True,
        help="Your model tester config file.",
    )
    args = parser.parse_args()
    logger.info("Reading config file: %s" % args.config)
    config = toml.load(args.config)
    t = Inference(config)
    t.predict()

    tdif = time() - t_start
    logger.info("Testing model done!")
    logger.info("Time used: %s" % str(timedelta(seconds=tdif)))


if __name__ == "__main__":
    main()
